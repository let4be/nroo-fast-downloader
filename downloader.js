var Promise			= require('bluebird'),
	crypto			= require('crypto');

var	co				= require('co'),
	sleep			= require('co-sleep'),
	chan			= require('chan'),
	fs				= Promise.promisifyAll(require('fs')),
	csv				= require('csv'),
	childProcess	= require('child_process'),
	db				= require('./lib/database').connect();

var inputFile	= process.argv[2],
	outputLog	= process.argv[3],
	threads		= process.argv[4],
	concurrency	= process.argv[5];

if (process.argv.length < 6) {
	console.error('use node --harmony downloader.js [input csv file] [output log file] [threads] [concurrency]');
	process.exit(1);
}

var finished = 0;
var childs = [];

var startTime = new Date();
var logStream = fs.createWriteStream(outputLog, {highWaterMark: 0, flags: 'a'});

process.on('uncaughtException', function (err) {
	console.error('uncaughtException in downloader', err.message, err.stack);
	process.exit(1);
});

function pad ( str, length, opts ) {
	var padding = ( new Array( Math.max( length - ( str + "" ).length + 1, 0 ) ) ).join( opts && opts.padWith || " " ),
		collapse = opts && opts.collapseEmpty && !( str + "" ).length;
	return collapse ? "" : opts && opts.padLeft ? padding + str : str + padding;
}

for(var i = 0; i < threads; i++) {
	console.log('spawn child process...');
	var child = childProcess.fork('./worker', [], {
		cwd: __dirname,
		silent: false
	});
	child.send({id: 'init', workerId: childs.length + 1, concurrency: concurrency});

	child.on('message', function (msg) {
		switch(msg.id) {
			case 'result':
				logStream.write((msg.remarks.isError ? '[er]' : '[ok]') +
						pad(msg.time, 6, {padWith: ' ', padLeft: true}) + 'ms. ' +
						pad(msg.alexaRank, 8, {padWith: ' ', padLeft: true}) + ' ' +
						msg.url + ' ' + msg.remarks.list.join('|') + '\n'
				);
				break;
			case 'finish':
				finished++;
				break;
		}
	});

	childs.push(child);
}

co(function *() {
	console.log('Loading tasks file...');

	var parser = csv.parse({
		columns: function () { return ['alexaRank', 'url'] }
	});

	fs.createReadStream(inputFile).pipe(parser);

	var tasksCh = chan();
	var tasksCntr = 0;
	var isEverythingSent = false;

	console.log('Reading tasks...');

	parser.on('readable', function () {
		var task;

		while (task = parser.read()) {
			try {
				if (!task.alexaRank || !task.url)
					continue;
				task.urlHash = crypto.createHash('sha1').update(task.url).digest('base64');

				tasksCntr++;
				tasksCh(task);
			} catch (err) {
				console.error('downloader: unhandled error while csv processing', err.message, err.stack);
			}
		}
	});

	parser.on('finish', function(){
		isEverythingSent = true;
	});

	co (function *() {
		var i = 0;
		while (!isEverythingSent || tasksCntr > 0) {
			try {
				var task = yield tasksCh;
				tasksCntr--;

				var urlEntry = yield db.Task.findOneAsync({urlHash: task.urlHash});

				if (urlEntry) {
					if (urlEntry.time && urlEntry.html) {
						var delta = Date.now() - urlEntry.time;
						if (delta < 1000 * 60 * 60 * 24) {
							console.log(task.url, task.alexaRank, 'SKIP - ALREADY PROCESSED', Math.floor(delta / 1000 / 60), 'min ago');
							continue;
						}
					}
				} else {
					var task = new db.Task({
						url: task.url,
						urlHash: task.urlHash,
						alexaRank: task.alexaRank
					});
					yield task.saveAsync();
				}

				childs[i++ % childs.length].send({id: 'queue', alexaRank: task.alexaRank, url: task.url, urlHash: task.urlHash});
			} catch (err) {
				console.error('downloader: unhandled error while task distribution', err.message, err.stack);
			}
		}

		console.log('Tasks are distributed amongst workers...');
		for(var i = 0; i < childs.length; i++)
			childs[i].send({id: 'finish'});
	});

	while (finished < childs.length)
		yield sleep(1000);

	console.log('Done!');

	var tookTime = new Date() - startTime;
	logStream.write('FINISHED FOR ' + tookTime + 'ms.' + '\n');

	process.exit();
});