This is fast download tool, written using javascript and node.js

1. install node(tested on 0.11.14):

	nvm install 0.11.14


2. increase the limits

	sudo sh -c "ulimit -n 65000 && ulimit -s 65000 && exec su $LOGNAME"


3. run the software

	nvm use 0.11.14
	
	export MONGOLAB_URI=mongodb://localhost/Downloader
	
	node --harmony --nouse-idle-notification --max-old-space-size=1024 downloader.js top-1m.csv ./downloader-log.txt 4 2048 2>&1 | tee ./node.log
	
	
4. access collected data

	currently software uses mongodb to save pages and keep track of their status/check time, to access the data connect to mongodb using
	
	mongo
	
	type "use Downloader" in order to select the database, and then issue your commands, for example:
				
	db.tasks.find({}) - find all pages
	
	db.tasks.find({status: 'error'}) - find pages with error
	
	db.tasks.count({status: 'error'}) - count pages with error
	
	
5. additional information

	increase limit for opened file descriptors
		ulimit -n 65000

	increase limit for stack size
		ulimit -s 65000

	execute node and use modern ES6 features
		node --harmony

	GC optimizations to speed up
		 --nouse-idle-notification
		 --max-old-space-size=1024

	downloader.js itself accepts 5 command line arguments

	top-1m.csv - input csv file [id, url without protocol]

	./downloader-log.txt - output file with status of each record from csv file

	4 - number of node.js instance(=number of cores)

	2048 - number of green threads inside each node.js instance(depends on speed of CPU and network bandwidth)

	2>&1 | tee ./node.log
	is optional and allows us to see tool output and save it to log file for further analysis and troubleshoot