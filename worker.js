var Promise		= require('bluebird'),
	crypto		= require('crypto');

var	fs			= Promise.promisifyAll(require('fs')),
	co			= require('co'),
	chan		= require('chan'),
	db			= require('./lib/database').connect(),
	Fetcher		= require('./lib/fetcher'),
	fetcher		= new Fetcher({
		userAgentString: 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36',
		timeoutMs: 60000
	});

var workerId = -1;
var concurrency = -1;

process.on('uncaughtException', function (err) {
	console.error('uncaughtException in worker', err.message, err.stack);
	process.exit(1);
});

var greenThread = function *(url, urlHash, remarks) {
	var prefix = 'worker [' + workerId  + ']';

	var urlOk = url;
	remarks.list = [];
	remarks.isError = false;
	try {
		var isRepeatWww = false;

		var res = null;
		while (true) {
			try {
				res = yield fetcher.get('http://' + url);
			} catch (err) {
				var errorCode = err && err.code ? err.code.toUpperCase() : '';
				if (errorCode == 'ENOTFOUND') {
					if (!isRepeatWww && url.indexOf('www.') != 0) {
						isRepeatWww = true;
						url = 'www.' + url;
						remarks.list.push('ENOTFOUND - retry with www.');
						remarks.isError = true;
						continue;
					}
				} else {
					console.error(prefix, url, 'RESPONSE_ERROR', err.message);
					remarks.list.push('RESPONSE_ERROR ' + err.message.replace(/(\r\n|\n|\r)/gm, ' '));
					remarks.isError = true;
				}
			}

			if (res) {
				urlOk = url;
				urlHash = crypto.createHash('sha1').update(url).digest('base64');
			}

			break;
		}

		if (!res)
			console.log(prefix, url, 'ERROR');
		else {
			remarks.list.push('ok');
			remarks.isError = false;

			console.log(prefix, url, 'DONE');
		}

		return {url: urlOk, urlHash: urlHash, result: res};
	} catch (err) {
		console.error(prefix, 'unhandled exception', err.message, err.stack);
		remarks.list.push('UNHANDLED ' + err.message);
		remarks.isError = true;
	}
};

var tasksCh = chan();
var tasksCntr = 0;
var isFinished = false;

process.on('message', function (m) {

	if (m.id == 'init') {
		console.log('[' + m.workerId + '] child instance started...')
		workerId = m.workerId;
		concurrency = m.concurrency;

		for(var i = 0; i < concurrency; i++)
			co(function *() {
				while (!isFinished || tasksCntr > 0) {
					var task = yield tasksCh;

					var res = null;
					var remarks = {};
					var startTime = new Date();
					try {
						res = yield greenThread(task.url, task.urlHash, remarks);
					} finally {
						var time = Date.now();
						var tookTime = time - startTime;

						try {
							yield db.Task.updateAsync(
								{urlHash: task.urlHash},
								{$set: {
									time: time,
									tookTime: tookTime,
									url: res.url,
									urlHash: res.urlHash,
									html: res.result,
									state: remarks.isError ? 'error' : 'ok'
								}, $pushAll: {
									lastErrors: remarks.list.map(function (e) {
										return {message: e, time: time}
									})
								}}
							);

							process.send({id: 'result', alexaRank: task.alexaRank, url: task.url, remarks: remarks, time: tookTime});
						} catch (err) {
							console.error('worker [' + m.workerId + '] error during saving processing results', err.message, err.stack);
						}

						tasksCntr--;
					}

					if (isFinished && tasksCntr < 1) {
						console.log('worker [' + workerId + '] done!');
						process.send({id: 'finish'});
						process.exit(0);
					}
				}
			});
	} else
	if (m.id == 'queue') {
		tasksCntr++;
		tasksCh(m);
	} else
	if (m.id == 'finish') {
		isFinished = true;

		if (tasksCntr < 1) {
			console.log('worker [' + workerId + '] done!');
			process.send({id: 'finish'});
			process.exit(0);
		}
	}
});