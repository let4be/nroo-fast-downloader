var
	Promise = require('bluebird');

var
	mongoose	= require('mongoose'),
	Schema 		= mongoose.Schema,
	ObjectId	= Schema.ObjectId;

if (!process.env.MONGOLAB_URI) {
	console.error('MONGOLAB_URI environment variable required!');
	process.exit(1);
}


var DataContext = function(db) {
	this.ObjectId = ObjectId;

	this.TaskSchema = new Schema({
		urlHash: {type: String, required: true},
		url: {type: String, required: true},
		time: {type: Date, default: null},
		tookTime: {type: Number, default: null},
		alexaRank: {type: Number, default: null},
		html: {type: String, default: ''},
		state: {type: String, default: 'queued', enum: ['queued', 'error', 'ok']},
		lastErrors: [{
			message: {type: String, required: true},
			time: {type: Date, required: true}
		}]
	});
	this.TaskSchema.index({urlHash: 1}, {unique: true});
	this.TaskSchema.index({url: 1, state: 1, time: 1, alexaRank: 1});
	//this.TaskSchema.index({html: 'text'});
	this.TaskSchema.set('autoIndex', true);

	this.Task = Promise.promisifyAll(db.model('Task', this.TaskSchema));

	Promise.promisifyAll(mongoose);
};


var Database = function () {
	var context = null;
	this.connect = function () {
		if (!context)
			context = new DataContext(mongoose.connect(process.env.MONGOLAB_URI.trim()));

		return context;
	};
};

module.exports = new Database();