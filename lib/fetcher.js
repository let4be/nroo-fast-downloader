var	constants		= require('constants'),
	chan			= require('chan'),
	url				= require('url'),
	http			= require('http'),
	https			= require('https'),
	request			= require('request');

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

https.globalAgent.options.secureOptions = constants.SSL_OP_ALLOW_UNSAFE_LEGACY_RENEGOTIATION | constants.SSL_OP_LEGACY_SERVER_CONNECT | constants.SSL_OP_DONT_INSERT_EMPTY_FRAGMENTS;
https.globalAgent.maxSockets = 30;
http.globalAgent.maxSockets = 30;

var Fetcher = function(options) {
	var self = this;

	if (!options.timeout)
		options.timeout = 10000;

	var req = request.defaults({
		headers: {
			'User-Agent': options.userAgentString
		},
		timeout: options.timeoutMs,
		followRedirect: true,
		followAllRedirects: true,
		encoding: null
	});

	self.get = function (uri) {
		var jar = request.jar();
		var ch = chan();

		var uriParsed = url.parse(uri);

		var requestObject = req.get({
			jar: jar,
			uri: uriParsed.href,
			headers: {
				Host: uriParsed.hostname
			}
		}, function(err, res, body) {
			if (err)
				ch(err);
			else
				ch(null, body);
		});
		requestObject.setMaxListeners(0);

		return ch;
	};
};

module.exports = Fetcher;